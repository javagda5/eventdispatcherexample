package com.sda.ed;

import com.sda.ed.events.IEvent;
import org.apache.commons.lang3.ClassUtils;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by amen on 12/17/17.
 */
public class EventDispatcher {

    private static final EventDispatcher instance = new EventDispatcher();
    private ExecutorService eventThread = Executors.newSingleThreadExecutor();

    public static EventDispatcher getInstance() {
        return instance;
    }

    private Map<Class<?>, List<Object>> listenersMap = new HashMap<>();

    public void register(Object registeredObject) {
        List<Class<?>> objecsInterfaces = ClassUtils.getAllInterfaces(registeredObject.getClass());
        for (Class<?> interfaceImplemented : objecsInterfaces) {
            List<Object> listOfObjectsImplementingInterface = listenersMap.get(interfaceImplemented);

            if (listOfObjectsImplementingInterface == null) {
                listOfObjectsImplementingInterface = new ArrayList<>();
            }
            listOfObjectsImplementingInterface.add(registeredObject);

            listenersMap.put(interfaceImplemented, listOfObjectsImplementingInterface);
        }
    }


    public void disptachEvent(IEvent event) {
        eventThread.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    event.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
//        eventThread.submit(event);
    }

    //    public List getAllObjectsImplementingInterface(Class<?> interfac) {
//        List list = listenersMap.get(interfac);
//        if (list == null) {
//            return new ArrayList();
//        }
//        return list;
//    }
    public Optional<List> getAllObjectsImplementingInterface(Class<?> interfac) {
        List list = listenersMap.get(interfac);

        return Optional.ofNullable(list);
    }

    public void print() {
        for (Class<?> interf : listenersMap.keySet()) {
            System.out.println("Interface: " + interf);
            for (Object o : listenersMap.get(interf)) {
                System.out.println("implemented by " + o);
            }
        }
    }
}
