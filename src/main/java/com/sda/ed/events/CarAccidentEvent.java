package com.sda.ed.events;

import com.sda.ed.EventDispatcher;
import com.sda.ed.listeners.ICarAccidentListener;

import java.util.*;

/**
 * Created by amen on 12/17/17.
 */
public class CarAccidentEvent implements IEvent {
    @Override
    public void run() {
        Optional<List> listeners = EventDispatcher.getInstance().getAllObjectsImplementingInterface(ICarAccidentListener.class);
//        if (listeners.isPresent()) {
//            List<ICarAccidentListener> listenersList = listeners.get();
//
//            for (ICarAccidentListener listener : listenersList) {
//                listener.carAccident();
//            }
//        }
        // list = ^^^^ listeners.get();
        listeners.ifPresent(list -> {
            List<ICarAccidentListener> listenersList = list;

            for (ICarAccidentListener listener : listenersList) {
                listener.carAccident();
            }
        });
    }
}
