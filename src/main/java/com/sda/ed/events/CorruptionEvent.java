package com.sda.ed.events;

import com.sda.ed.EventDispatcher;
import com.sda.ed.listeners.ICarAccidentListener;
import com.sda.ed.listeners.ICorruptionListener;

import java.util.List;
import java.util.Optional;

/**
 * Created by amen on 12/17/17.
 */
public class CorruptionEvent implements IEvent {
    private String who;
    private String where;
    private String withWho;

    public CorruptionEvent(String who, String where, String withWho) {
        this.who = who;
        this.where = where;
        this.withWho = withWho;
    }

    public CorruptionEvent() {
    }

    @Override
    public void run() {
        Optional<List> listeners = EventDispatcher.getInstance().getAllObjectsImplementingInterface(ICorruptionListener.class);

        listeners.ifPresent(list -> {
            List<ICorruptionListener> listenersList = list;

            if (where == null && who == null && withWho == null) {
                for (ICorruptionListener listener : listenersList) {
                    listener.corruptionCommited();
                }
            } else {
                for (ICorruptionListener listener : listenersList) {
                    listener.corruptionCommited(who, where, withWho);
                }
            }
        });
    }
}
