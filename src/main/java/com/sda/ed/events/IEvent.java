package com.sda.ed.events;

/**
 * Created by amen on 12/17/17.
 */
public interface IEvent {
    void run();
}
