package com.sda.ed.models;

import com.sda.ed.listeners.ICarAccidentListener;
import com.sda.ed.listeners.ICorruptionListener;
import com.sda.ed.listeners.IMurderListener;

/**
 * Created by amen on 12/17/17.
 */
public class Journalist implements IMurderListener, ICorruptionListener, ICarAccidentListener {
    private String name;

    public Journalist(String name) {
        this.name = name;
    }

    @Override
    public void murderCommited() {

    }

    @Override
    public void corruptionCommited() {
        System.out.println("Corruption again!");
    }

    @Override
    public void carAccident() {
        System.out.println("Journalist " + name + " is informed about car accident");
    }

    @Override
    public void corruptionCommited(String who, String where, String withWho) {
        System.out.println("Corruption ! " + where + " " + who + " " + withWho);
    }

    @Override
    public String toString() {
        return "Journalist{" +
                "name='" + name + '\'' +
                '}';
    }
}
