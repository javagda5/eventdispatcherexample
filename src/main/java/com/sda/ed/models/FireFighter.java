package com.sda.ed.models;

import com.sda.ed.listeners.ICarAccidentListener;
import com.sda.ed.listeners.IMurderListener;

/**
 * Created by amen on 12/17/17.
 */
public class FireFighter implements ICarAccidentListener {
    private String name;

    public FireFighter(String name) {
        this.name = name;
    }

    @Override
    public void carAccident() {
        System.out.println("FireFighter " + name + " is informed about car accident" );
    }

    @Override
    public String toString() {
        return "FireFighter{" +
                "name='" + name + '\'' +
                '}';
    }
}
