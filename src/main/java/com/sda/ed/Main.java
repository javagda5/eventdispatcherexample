package com.sda.ed;

import com.sda.ed.events.CarAccidentEvent;
import com.sda.ed.events.CorruptionEvent;
import com.sda.ed.models.FireFighter;
import com.sda.ed.models.Journalist;

/**
 * Created by amen on 12/17/17.
 */
public class Main {
    public static void main(String[] args) {

        EventDispatcher.getInstance().register(new Journalist("Jurek"));
        EventDispatcher.getInstance().register(new Journalist("Marian"));
        EventDispatcher.getInstance().register(new FireFighter("Rurek"));
        EventDispatcher.getInstance().register(new FireFighter("Bogdan"));

//        EventDispatcher.getInstance().print();

        EventDispatcher.getInstance().disptachEvent(new CarAccidentEvent());

        EventDispatcher.getInstance().disptachEvent(new CorruptionEvent("Paweł", "Gdańsk", "Gaweł"));

        EventDispatcher.getInstance().disptachEvent(new CorruptionEvent());

    }
}
