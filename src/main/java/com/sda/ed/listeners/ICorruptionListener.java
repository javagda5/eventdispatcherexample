package com.sda.ed.listeners;

/**
 * Created by amen on 12/17/17.
 */
public interface ICorruptionListener {
    void corruptionCommited();
    void corruptionCommited(String who, String where, String withWho);
}
